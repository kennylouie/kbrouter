package kbrouter

import (
	"testing"
)

func TestFunc(t *testing.T) {
	tests := []funcTest{
		funcTest{
			title: "dummy func",
			path:  "test",
			function: func() error {
				return nil
			},
			response: nil,
		},
	}

	for _, test := range tests {
		t.Run(test.title, func(t *testing.T) {
			testFunc(t, test)
		})
	}
}

func testFunc(t *testing.T, test funcTest) {
	r := NewRouter().HandleFunc(test.path, test.function)

	route := r.First()
	if route == nil {
		t.Errorf("expected routes in router")
	}

	if route.Path != test.path {
		t.Errorf("expected %v, got but %v", test.path, route.Path)
	}

	if actual := route.Handler(); actual != test.response {
		t.Errorf("expected %v, got but %v", test.response, actual)
	}
}

type funcTest struct {
	title    string
	path     string
	function func() error
	response error
}
