package kbrouter

import (
	"strings"
)

type Router struct {
	Routes []*Route
}

type Route struct {
	// Path is composed of a command args
	// e.g. if a keybase message is:
	// _kbbot arg1 arg2 arg3
	// then the command is arg1
	// and the args are the remainder
	// e.g. Path:
	// todos action todo
	Path string

	// m message from keybase
	// p route path to parse against
	Handler func() error
}

func NewRouter() *Router {
	return &Router{}
}

// adds a route to the router
func (r *Router) HandleFunc(path string, handler func() error) *Router {
	r.Routes = append(r.Routes, &Route{
		Path: path,
		Handler: handler,
	})

	return r
}

// gets the first route
func (r Router) First() *Route {
	if r.Empty() {
		return nil
	}

	return r.Routes[0]
}

func (r Router) Empty() bool {
	return len(r.Routes) == 0
}

func (r Router) FindRoute(command string) *Route {
	if r.Empty() {
		return nil
	}

	for _, route := range r.Routes {
		if strings.Split(route.Path, " ")[0] == command {
			return route
		}
	}

	return nil
}
